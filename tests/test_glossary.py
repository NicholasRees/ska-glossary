#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the ska_python_skeleton module."""
import pytest
import json
from unittest import mock
from ska.glossary.glossary import glossary,getauth
import os

# Populate the test glossary
terms = []
filename=os.path.join(os.path.dirname(os.path.abspath(__file__)),'glossary.json')
with open(filename, 'r' ) as fp:
    terms = json.load( fp )

# This method will be used by the mock to replace requests.get
def mocked_requests_get(*args, **kwargs):
    class MockResponse:

        def __init__(self, status_code, result):
            self.status_code = status_code
            self.text = json.dumps( result )

        def json(self):
            return json.loads(self.text)

    if (args[0] == 'https://confluence.skatelescope.org/rest/glossary/1.0/term/search' and 'params' in kwargs ):
        p = {
            'query': '',
            'spaces': '',
            'synonyms': '',
            'abbreviations': '',
            'labels': '',
            'creators': '',
            'creation_date': '',
            'date_value': '',
            'date_from': '',
            'date_to': '',
            'offset_value': 0,
            'limit_value': len(terms),
            'order': 'termASC'
        }

        params = kwargs['params']
        for key in p.keys():
            if key in params:
                p[key] = params[key]
                
        result = {
            'results_count' : len(terms),
            'limit' : p['limit_value'],
            'offset' : p['offset_value'],
            'terms_list' : terms[p['offset_value']:(p['offset_value']+p['limit_value']-1)]
        }
        return MockResponse(200, result )

    return MockResponse(404,None)


# Now we must patch 'ska.glossary.requests.get'

# TODO: Replace all the following examples with tests for the ska_python_skeleton package code
@mock.patch('ska.glossary.glossary.requests.get', new=mocked_requests_get)
def test_something():
    """Example: Assert with no defined return value."""
    g=glossary( auth=getauth('confservicesccount'))
    assert True
