SKA Glossary Tools
==================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-templates-ska-python-skeleton/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-python-skeleton/en/latest/?badge=latest)

This package contains code for synchronising the SKA glossary and acronym list across various locations.