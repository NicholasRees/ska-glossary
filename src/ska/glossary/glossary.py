# -*- coding: utf-8 -*-

#"""Define function placeholders and test function examples."""


# TODO: Replace all the following code with the desired functionality for the package
import requests
import json
import sys

def getauth(username):
    """Function to get a authorisation credentials from a private file kept in
     ~/.ssh/<username>"""
    import pathlib
    filename = pathlib.PurePath(pathlib.Path.home(), '.ssh', username)
    with open(filename, 'r') as fd:
        password = fd.readline().strip()

    return (username, password)


# TODO: Replace all the following code with the desired functionality for the package
class glossary:
    """Define class, methods etc"""

    glossary = {}

    def __init__(
        self,
        url='https://confluence.skatelescope.org',
        space='GLOS',
        auth=('username', 'password') ) :

        search_query = url + '/rest/glossary/1.0/term/search'
        parameters = {
            'query': '',
            'spaces': space,
            'synonyms': '',
            'abbreviations': '',
            'labels': '',
            'creators': '',
            'creation_date': '',
            'date_value': '',
            'date_from': '',
            'date_to': '',
            'offset_value': 0,
            'limit_value': 100,
            'order': 'termASC'
        }
        # Get the JSON String
        try:
            r = requests.get(search_query, auth=auth, params=parameters)
            result = json.loads(r.text)
            """ result=r.json()
            transtable = str.maketrans({';':' ', ',':' '})

            for entry in result['terms_list']:
                title = entry.pop('title')
                abbr  = entry.pop('abbreviations')
                synonyms = entry.pop('synonyms')
                glossary[title] = {
                    'creator': entry['creator'],
                    'abbreviations': abbr.translate(transtable).split(),
                    'synonyms': synonyms.translate(transtable).split(),
                    'labels' : entry['labels'],
                    'dateCreation' : datetime.datetime.strptime(entry['dateCreation'],'%d-%m-%Y (%H:%M)')
                } """

            self.glossary = result['terms_list']

            if (result['results_count'] > parameters['limit_value']):
                parameters['offset_value']= parameters['limit_value']
                parameters['limit_value'] = result['results_count'] - parameters['limit_value']
                r = requests.get(search_query, auth=auth, params=parameters)
                result = json.loads(r.text)
                self.glossary += result['terms_list']
        except:
            print( 'Error initialising Glossary\n' )
            print( r+'\n' )
            print( result+'\n' )
            print( glossary+'\n' )

    def printTerms(self):
        """Print list of terms"""

        for term in self.glossary:
            print( term['title'] )

    def dump( self, fp ):
        json.dump( self.glossary, fp, indent=4,sort_keys=True )

if __name__ == '__main__':
    g=glossary( auth=getauth('confservicesccount'))
    g.dump(sys.stdout)